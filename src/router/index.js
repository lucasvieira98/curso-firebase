import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    meta: { icon: 'home', title: 'Home' },
    component: () => import(/* webpackChunkName: "home" */ '../views/Home.vue')
  },
  {
    path: '/expenses-list',
    name: 'expenses-list',
    meta: { icon: 'list-ul', title: 'Lista Gastos' },
    component: () => import(/* webpackChunkName: "lista-gastos" */ '../views/expenses-list/ExpensesList.vue')
  },
  {
    path: '/login',
    name: 'Login',
    meta: { icon: 'home', title: 'Login' },
    component: () => import(/* webpackChunkName: "login" */ '../views/Login.vue')
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

router.beforeEach((to, from, next) => {
  document.title = `${to.meta.title} - Expenses`

  if (!window.uid && to.name !== 'Login') {
    next({ name: 'Login' })
  } else {
    next()
  }
})

export default router
